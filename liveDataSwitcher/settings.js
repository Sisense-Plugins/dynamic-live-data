
//	Plugin configuration
var settings = {
	liveDataSetSearch: 'customers',
	queryStringParam: 'ds',
	label: {
		long: 'Change Live Data Source',
		short: 'Change Source'
	}
}
