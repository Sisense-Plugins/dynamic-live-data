
prism.run(["$jaql", function ($jaql) {	

	/****************************************/
	/*	Initial Dashboard Load Functions 	*/
	/****************************************/

	//	Find only the matching LIVE data sources, and set the dashboard to the first match
	function elasticubeInfoHandler(datasources){

		//	Get a list of matching live sources (save for later)
		prism.liveSources = datasources.filter(function(d) { 
			return d.live && d.title.startsWith(settings.liveDataSetSearch)
		});

		//	Make sure we have access to some live sources
		var canChange = (prism.liveSources.length>0) && prism.$ngscope.dashboard.datasource.live;
		if (canChange){

			//	Instantiate a variable for the new live source
			var newLiveSource = null;

			//	How many live sources does the user have access to?
			if (prism.liveSources.length > 1) {

				//	Get the name of the preferred live source
				var preferredSource = getUrlParameter();
				var match = prism.liveSources.filter(function(ds){
					return ds.title == preferredSource;
				})
				if (match.length == 1) {
					//	Found a match
					newLiveSource = match[0];
				}else {
					//	Match not found, just grab the first
					newLiveSource = prism.liveSources[0];
				}

			} else {
				//	Only has access to the one source
				newLiveSource = prism.liveSources[0];
			}

			//	Run the function to set a new data source
			changeSource(newLiveSource)
		}
	}	

	// Function to get the list of available data sources
	function elasticubeSwitcher(dashboard, args) {

		//	Figure out the API call url
		var elasticubeInfoUrl = '/api/datasources/';

		//	Make the API calls
		$.ajax({
			async:false,
			url: elasticubeInfoUrl,
			method: 'GET'
		}).done(elasticubeInfoHandler);
	}


	/****************************************/
	/*	Live Datasource Switcher Functions 	*/
	/****************************************/

	//	Function to switch the data source
	function changeSource(newDatasource){

		//	Get the dashboard
		var dashboard = prism.$ngscope.dashboard;
	
		//	Get the new datasource to set
		var ds = newDatasource ? newDatasource : this.datasource;
		var oldDs = typeof dashboard.datasource === "string" ? dashboard.datasource : dashboard.datasource.title;

		//	Only run if we are actually making a change
		if (ds.title !== oldDs) {

			//	Set the data source for the whole dashboard
			dashboard.datasource = ds;

			//	Update the widgets individually
			$.each(dashboard.widgets.$$widgets, function(){

				//	Update the datasource of the widget
				this.datasource = ds;

				//	Loop through each panel
				this.metadata.panels.forEach(function(panel){

					//	Loop through each panel's items
					panel.items.forEach(function(item){

						//	Recursively check the jaql to replace all instances of the dim/table properties
						jaqlDigger(item.jaql, ds.title, oldDs)
					})
				})
			});

			//	Refresh the dashboard
			dashboard.refresh();
		}
	}

	//	Function to recursively iterate through the jaql to see if properties need to be changed
	function jaqlDigger(obj,newTableName, oldTableName){

		//	Make sure this jaql is valid
		var isValid = (typeof obj == "object" || !$.isEmptyObject(obj));
		if (isValid){
			//	Valid object, iterate through each property
			for (key in obj){
				//	Make sure the obj[key] is valid
				if (obj.hasOwnProperty(key)){
					//	Check the data type
					if (typeof obj[key] == "object"){
						//	This is a sub-object, need to recurse
						jaqlDigger(obj[key],newTableName,oldTableName);
					} else if (typeof obj[key] == "string"){
						//	This is a normal string data type, check to see if we need to change it
						if (key === "table") {
							//	Update the table attribute
							obj[key] = newTableName;
						} else if (key == "dim"){
							//	Update the dim attribute
							obj[key] = obj[key].replace("[" + oldTableName + ".", "[" + newTableName + ".");
						}
					}
				}
			}
		}
	}


	/****************************************/
	/*	Utility Functions					*/
	/****************************************/

	//	Function to handle the menu popup
	function initMenu(event, args){

		//	Make sure this only runs on the dashboard settings menu
		var isRightMenu = isValid(args.settings);
		if (isRightMenu){

			//	Define a separator
			var separator = {
				'type': 'separator'
			}

			//	Define the new menu item
			var newItem = {
				'caption': settings.label.short,
				'desc': settings.label.long,
				'disabled': false,
				'drillRight': true,
				'id': 'liveDataSwitcher',
				'tooltip': settings.label.long,
				'margin': 3,
				'items': []
			}

			//	Add the items, based on the available live sources
			prism.liveSources.forEach(function(s){

				//	Create the menu item
				var subItem = {
					'caption': s.title,
					'disabled': false,
					'tooltip': s.title,
					'desc': s.title,
					'datasource': s,
					'execute': changeSource
				}

				//	Add to the overall menu
				newItem.items.push(subItem);
			})

			//	Save to the existing items list
			args.settings.items.push(separator);
			args.settings.items.push(newItem);
		}
	}

	//	Function to check if this is the right menu
	function isValid(settings){
		if (settings.name == "dashboard"){
			return true;
		} else {
			return false;
		}
	}

	//	Function to check for a ds query string parameter
	function getUrlParameter() {
		//	Get the name of the query string parameter
	    var name = settings.queryStringParam.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	    //	Create a regex
	    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	    //	Search everything past the hash
	    var results = regex.exec(location.hash);
	    //	Return the results
	    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
	};


	/****************************************/
	/*	Event Handlers 						*/
	/****************************************/

	prism.on("dashboardloaded", elasticubeSwitcher);
	prism.on("beforemenu", initMenu);


}]);


