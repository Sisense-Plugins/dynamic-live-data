#	Live Data Switcher Plugin

This plugin was created to decide on the fly, which Live Data Source should be used to power a given dashboard.  This plugin runs whenever a dashboard is loaded that references a Live Data Source, and makes an API call to see what Live Data Sources are available.  This list of Live Data Sources includes only sources made accessible through Access Rights on the Admin page, which ensures users can only see specific Live Data Sources.  Once the plugin gets this list of Live Data Sources, it updates the dashboard to load the first one it gets back.  If a user has access to only a single Live Data Source, it will be set automatically.  

In some use cases, you may want to pass the name of the Live Data Source as part of the dashboard URL.  This plugin checks for a querystring parameter named ds, and will try to set the selection as the data source for the dashboard.  In order to work, this does require the end user having access rights to a Live Data Source with the same name. 


![alt Setting Live Data Source from QueryString](screenshots/querystring.png) 

 
In other use cases, you may have a user with access to several Live Data Sources.  There is a new option in the Dashboard's menu dropdown for selecting the Live Data Source to use. 


![alt Changing the data source from menu](screenshots/changeMenu.png)

 
An important note is that this implementation is just an example of how the business logic can be implemented.  All business logic for deciding what Live Data Source should be loaded is contained in main.js.  

__Step 1: Copy the plugin__ - Copy the liveDataSwitcher folder into your C:\program files\sisense\PrismWeb\plugins directory.

__Step 2: Add Security__ - This plugin simply redirects queries from 1 Live Data Source to another.  It is **critical** to also apply Access Rights based on theses user/groups, so that if anything goes wrong, you don't accidentally show data from other Live Sources.  In order to setup Access Rights on Live Data Sources, please see the documentation here

https://documentation.sisense.com/security/#Assign_rights_EC

# Notes
* This sample has been confirmed working on Sisense version 7.1, and should be backwards compatible with previous version.
* This plugin assumes the Live Data Sources to switch between have already been created (either through the Admin page or via REST API)
* This plugin also assumes the various Live Data Sources have the same table/view structure (same field names)
